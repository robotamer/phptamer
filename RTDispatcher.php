<?php

class RTDispatcher
{
	protected $file;
	protected $module;
	protected $controller;
	protected $action;

	function __construct()
	{
		$this->segments = RTUri::getEnvSegments();
		$this->setController();
		$this->setFile('bin');
	}

	function run()
	{
		if (file_exists($this->file)) {
			include $this->file;
		}else if (file_exists($this->setFile('sys'))) {

			include $this->file; // sys file
			$this->setAction();
			S::set(new $this->controller(),'Controller');
			if( method_exists(S::Controller(), $this->action) ){
				call_user_func(array (S::Controller(), $this->action));
			}else{
				S::RTLogger('Sys Controller: '.$this->controller.' Action:'.$this->action.' not found!','N');
				header('Location: /404.php');
				exit;
			}
		}else{
			S::RTLogger('RTDispatcher SYS or BIN Controller: '.$this->controller.' not found!','N');
			header('Location: /404.php');
			exit;
		}
	}


	function setController()
	{
		$this->controller = FALSE;
		if(!empty($this->segments[0])){
			$this->controller = ucwords($this->segments[0]);
		}
	}

	function setFile($dir)
	{
		$c = $this->controller == FALSE ? 'Lobby' : $this->controller;
		return $this->file = ROOT . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $c . '.php';
	}

	function setAction()
	{
		if (isset($this->segments[1])) {
			$this->action = strtolower($this->segments[1]);
		}else{
			$this->action = 'lobby';
		}
	}
}
?>
