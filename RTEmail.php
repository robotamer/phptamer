<?php

/*
 *  @author     Dennis T Kaplan
 *  @copyright  Copyright (c) 2008 - 2012, Dennis T Kaplan
 *  @license    http://robotamer.bitbucket.org/html/PHPTamer/License.html
 *  @link       http://robotamer.bitbucket.org/html/PHPTamer/
 */
/**
 * Description of RTEmail
 *
 * @author RoboTamer
 */
class RTEmail
{

	protected static $from;
	protected static $to;
	protected static $subject;
	protected static $text;
	protected static $html;

	function setSubject($string)
	{
		if (strlen($string) > 255) {
			trigger_error('Invalid length for email subject.', E_USER_ERROR);
		} else {
			self::$subject = $string;
		}
	}

	function send($from, $to, $subject, $text, $html = NULL)
	{
		$boundary = md5(date('U'));

		$to = $email;
		$subject = "My Subject";

		$headers = "From: myaddress@mydomain.com" . "\r\n" .
		"X-Mailer: PHP/" . phpversion() . "\r\n" .
		"MIME-Version: 1.0" . "\r\n" .
		"Content-Type: multipart/alternative; boundary=--$boundary" . "\r\n" .
		"Content-Transfer-Encoding: 7bit" . "\r\n";

		$text = "You really ought remember the birthdays";
		$html = '<html>
        <head>
          <title>Birthday Reminders for August</title>
        </head>
        <body>
          <p>Here are the birthdays upcoming in August!</p>
          <table>
            <tr>
              <th>Person</th><th>Day</th><th>Month</th><th>Year</th>
            </tr>
            <tr>
              <td>Joe</td><td>3rd</td><td>August</td><td>1970</td>
            </tr>
            <tr>
              <td>Sally</td><td>17th</td><td>August</td><td>1973</td>
            </tr>
          </table>
        </body>
        </html>
        ';

		$message = "Multipart Message coming up" . "\r\n\r\n" .
		"--" . $boundary .
		"Content-Type: text/plain; charset=\"iso-8859-1\"" .
		"Content-Transfer-Encoding: 7bit" .
		$text .
		"--" . $boundary .
		"Content-Type: text/html; charset=\"iso-8859-1\"" .
		"Content-Transfer-Encoding: 7bit" .
		$html .
		"--" . $boundary . "--";



		mail($to, $subject, $message, $headers);
	}

}
?>
