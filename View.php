<?php

class View
{
    public function __construct($proxy){
		
		if($proxy == 'Aura'){
			$this->proxy = new View_Aura();
		}else{
			$this->proxy = new View_Zend();
		}
		$this->proxy = require PHPL . '/composer/vendor/Aura/View/scripts/instance.php';
	}
    
    /**
     * 
     * Magic read access to template data.
     * 
     * @param string $key The template variable name.
     * 
     * @return mixed
     * 
     */
    public function __get($key)
    {
        return $this->proxy->get($key);
    }
    
    /**
     * 
     * Magic write access to template data.
     * 
     * @param string $key The template variable name.
     * 
     * @param string $val The template variable value.
     * 
     * @return mixed
     * 
     */
    public function __set($key, $val)
    {
		$this->proxy->set($key, $val);
    }
    
    /**
     * 
     * Magic isset() checks on template data.
     * 
     * @param string $key The template variable name.
     * 
     * @return bool
     * 
     */
    public function __isset($key)
    {
		return $this->proxy->isset($key);
    }
    
    /**
     * 
     * Magic unset() for template data.
     * 
     * @param string $key The template variable name.
     * 
     * @return void
     * 
     */
    public function __unset($key)
    {
        $this->proxy->unset($key);
    }
    
    /**
     * 
     * Magic call to provide shared helpers as template methods.
     * 
     * @param string $name The helper name.
     * 
     * @param array $args The arguments to pass to the helper.
     * 
     * @return void
     * 
     */
    public function __call($name, $args)
    {
		return $this->proxy->call($name, $args);
    }
    
    /**
     * 
     * Sets the search paths for templates; paths are searched in FIFO order.
     * 
     * @param array $paths An array of directory paths where templates are.
     * 
     * @return void
     * 
     */
    public function setPaths(array $paths = array())
    {
        $this->_template_finder->setPaths($paths);
    }
    
    /**
     * 
     * Merges new data with the existing template data.
     * 
     * @param array $data An array of key-value pairs where the keys are 
     * template variable names, and the values are the variable values.
     * 
     * @return void
     * 
     */
    public function addData(array $data = array())
    {
        $this->_data    = (object) array_merge((array) $this->_data, $data);
        $this->_escaper = $this->_escaper_factory->newInstance($this->_data);
    }
    
    /**
     * 
     * Replaces all template data at once; this will remove all previous
     * data.
     * 
     * @param array $data An array of key-value pairs where the keys are 
     * template variable names, and the values are the variable values.
     * 
     * @return void
     * 
     */
    public function setData(array $data = array())
    {
        $this->_data    = (object) $data;
        $this->_escaper = $this->_escaper_factory->newInstance($this->_data);
    }
    
    /**
     * 
     * Gets all template variables.
     * 
     * @return array An array of key-value pairs where the keys are 
     * template variable names, and the values are the variable values.
     * 
     */
    public function getData()
    {
        return (array) $this->_data;
    }
    
    public function __raw()
    {
        return $this->_data;
    }
    
    /**
     * 
     * Returns the helper locator object.
     * 
     * @return HelperLocator
     * 
     */
    public function getHelperLocator()
    {
        return $this->_helper_locator;
    }
    
    /**
     * 
     * Returns the path to the requested template script; searches through
     * $this->paths to find the first matching template.
     * 
     * @param string $name The template name to look for in the template path.
     * 
     * @return string The full path to the template script.
     * 
     */
    public function find($name)
    {
        // append ".php" if needed
        if (substr($name, -4) != '.php') {
            $name .= '.php';
        }
        
        // find the path to the template
        $file = $this->_template_finder->find($name);
        if (! $file) {
            throw new Exception\TemplateNotFound($name);
        }
        
        // done!
        return $file;
    }
    
    /**
     * 
     * Returns the TemplateFinder object.
     * 
     * @return TemplateFinder
     * 
     */
    public function getTemplateFinder()
    {
        return $this->_template_finder;
    }
    
    /**
     * 
     * Retrieves a shared helper from the helper container.
     * 
     * @param string $name The helper to retrieve.
     * 
     * @return mixed
     * 
     */
    public function getHelper($name)
    {
        return $this->_helper_locator->get($name);
    }
    
    /**
     * 
     * Fetches the output from a template.
     * 
     * @param string $__name__ The template name to use.
     * 
     * @param array $vars Variables to extract into the local scope.
     * 
     * @return string
     * 
     */
    abstract public function fetch($__name__);

    /**
     * 
     * Fetches the output from a partial. The partial will be executed in
     * isolation from the rest of the template, which means `$this` refers
     * to the *partial* data, not the original template data. However,
     * helper objects are shared.
     * 
     * @param string $name The partial to use.
     * 
     * @param array $data Data to use for the partial.
     * 
     * @return string
     * 
     */
    abstract public function partial($name, array $data = array());
}
