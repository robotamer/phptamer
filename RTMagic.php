<?php

//trait RTMagic {
abstract class RTMagic {
	/**
	 * Our array of options
	 * @access protected
	 */
	protected static $data;

	public static function set($key = NULL, $value = NULL) {
		if($key === NULL){
			self::$data[] = $value;
			return true;
		}
		if (is_string($value) || is_int($value)){
			self::$data[$key] = $value;
			return true;
		}elseif ($value === NULL && is_array($key)){
			foreach($key as $k => $v){
				self::set($k, $v);
			}
		}
		return false;
	}

	public static function has($key) {
		return isset(self::$data[$key]) ? true : false;
	}

	public static function get($key = null) {
		if($key === null){
			return self::$data;
		}
		return isset(self::$data[$key]) ? self::$data[$key] : null;
	}

	public static function count(){
		if(isset(self::$data) && is_array(self::$data)){
			return count(self::$data);
		}
		return 0;
	}

	public static function del($key) {
		if (isset(self::$data[$key])) {
			unset(self::$data[$key]);
			return TRUE;
		}
		return FALSE;
	}

	public function __set($key, $value) {
		self::set($key, $value);
	}

	public function __get($key) {
		self::get($key);
	}

	public function __isset($key) {
		return self::has($key);
	}

	public function __unset($key) {
		self::del($key);
	}

} // End TaMeR FrameWork Core Class
?>
