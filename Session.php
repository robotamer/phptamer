<?php

/**
 * The RoboTamer Session
 * 
 * 
 * The MIT License (MIT)
 * 
 * Copyright © 2012 RoboTamer http://robotamer.github.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. 
 */

namespace RoboTamer;

/**
 * Session
 * Administer session and session flash data.
 * 
 * @category   Session
 * @package    RoboTamer
 * @author     Dennis T Kaplan
 * @copyright  Copyright (c) 2008 - 2012, Dennis T Kaplan
 * @license    http://robotamer.github.com
 * @link       http://robotamer.github.com
 */
class Session {

	public function __construct () {
		
		if (!self::started ())
			session_start ();
		if (!isset ($_SESSION[':new:']))
			$_SESSION[':new:'] = NULL;
	}

	public function __destruct () {
		
		$_SESSION[':old:'] = $_SESSION[':new:'];
		unset ($_SESSION[':new:']);
	}

	public static function started () {
		
		return isset ($_SESSION) ? TRUE : FALSE;
	}

	public static function __set ($name, $value) {
		
		$_SESSION[$name] = $value;
	}

	public static function __get ($name) {
		
		if (array_key_exists ($name, $_SESSION[$name])) {
			return $_SESSION[$name];
		}
	}

	public static function __isset ($name) {
		
		return isset ($_SESSION[$name]);
	}

	public static function __unset ($name) {
		
		unset ($_SESSION[$name]);
	}

	/**
	 * Write an item to the session flash data.
	 *
	 * Flash data only exists for the current and next request.
	 *
	 * <code>
	 *      // Write an item to the session flash data
	 *      Session::flash('name', 'Bob');
	 * </code>
	 *
	 * @param  string  $key
	 * @param  mixed   $value
	 * @return void
	 */
	public static function flash ($key, $value) {
		
		$_SESSION[':new:'][$key] = $value;
	}

	/**
	 * Keep all of the session flash data from expiring after the request.
	 *
	 * @return void
	 */
	public static function reflash () {
		
		$old = $_SESSION[':old:'];
		unset ($_SESSION[':old:']);
		$_SESSION[':new:'] = array_merge ($_SESSION[':new:'], $old);
	}

	/**
	 * Keep a session flash item from expiring at the end of the request.
	 *
	 * <code>
	 *      // Keep the "name" item from expiring from the flash data
	 *      Session::keep('name');
	 *
	 *      // Keep the "name" and "email" items from expiring from the flash data
	 *      Session::keep(array('name', 'email'));
	 * </code>
	 *
	 * @param  string|array  $keys
	 * @return void
	 */
	public static function keep ($keys) {
		
		foreach ((array) $keys as $key) {

			if (isset ($_SESSION[':new:'][$key])) {
				$this->flash ($key, $_SESSION[':new:'][$key]);
			} elseif (isset ($_SESSION[':old:'][$key])) {
				$this->flash ($key, $_SESSION[':old:'][$key]);
				unset ($_SESSION[':old:'][$key]);
			}
		}
	}

}

?>
