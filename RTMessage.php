<?php

// TaMeR Message
/**
 * @category	RoboTamer
 * @package		Message
 * @copyright	Copyright (c) 2008 - 2011, Dennis T Kaplan
 * @license		http://robotamer.com/license.html
 * @link		http://robotamer.com
 * ******************************************************* */
/**
 * $msg = Singleton::factory('Message');
 *      OR
 * $msg = new Message();
 * $msg->add('Info', 'I');
 * $msg->add('Error','E');
 * $msg->add('Warn', 'W');
 * $msg->add('Success','S');
 * $msg->add('Stop', 'D');
 * echo $msg->get();
 *
 * @category	RoboTamer
 * @package		Message
 * @author		Dennis T Kaplan
 * @todo		Move style to css file
 */
class RTMessage
{
	const success = '<span class="label label-success"><img src="/assets/ico/success.gif" alt="Success: " /> Success</span>';
	const info = '<span class="label label-info"><img src="/assets/ico/info.gif" alt="Info: " /> Info</span>';
	const warn = '<span class="label label-warning"><img src="/assets/ico/warn.gif" alt="Warning: " /> Warning</span>';
	const error = '<span class="label label-important"><img src="/assets/ico/error.gif" alt="Error: " /> Error</span>';
	const stop = '<span class="label label-important"><img src="/assets/ico/stop.gif" alt="Stop: " /> Important</span>';
	const red = '#E86F7A';
	const green = '#A4EDA8';
	const yellow = '#EBE790';
	const blue = '#9690EB';
	public static function clear()
	{
		$_SESSION['msg'] = array ();
	}

	public static function gethtml()
	{
		if (!empty($_SESSION['msg'])) {
			$html = '<div>' . PHP_EOL;
			foreach ($_SESSION['msg'] as $message) {
				$html .= self::makehtml($message);
			}
			$html .= '</div>' . PHP_EOL;
			unset($_SESSION['msg']);
			return $html;
		}
	}

	public static function add($message, $type = 'E')
	{
		$string = "$message,$type";
		if (defined('SID')) {
			if (empty($_SESSION['msg'])) {
				$_SESSION['msg'] = array ();
			}
			$_SESSION['msg'][count($_SESSION['msg'])] = $string;
		} else {
			S::RTRegistry()->set('msg', S::RTRegistry()->count('msg'), $string);
		}
	}

	public static function has()
	{
		if (defined('SID') && !empty($_SESSION['msg'])) {
			return true;
		} else {
			return S::RTRegistry()->has('msg');
		}
		return false;
	}

	public static function get()
	{
		if (defined('SID')) {
			if (!empty($_SESSION['msg'])) {
				$messages = $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
		} else {
			$messages = S::RTRegistry()->get('msg');
			S::RTRegistry()->del('msg');
		}
		$return = array ();
		foreach ($messages as $message) {
			$return[] = self::make($message);
		}
		return $return;
	}

	public static function make($message)
	{
		list($text, $type) = explode(',', $message);

		switch (strtoupper($type)):
			case 'I':
				$type = 'Info';
				break;

			case 'W':
				$type = 'Warn';
				break;

			case 'N':
				$type = 'Notice';
				break;

			case 'E':
				$type = 'Error';
				break;

			case 'D':
				$type = 'Stop';
				break;

			case 'S':
				$type = 'Success';
				break;
		endswitch;

		return $text . ',' . $type;
	}

	public static function makehtml($message)
	{
		list($text, $type) = explode(',', $message);
		$col = $img = $name = '';
		switch ($type):
			case 'I':
				$type = 'Info';
				$col = self::blue;
				$img = self::info;
				break;

			case 'W':
				$type = 'Warn';
				$col = self::yellow;
				$img = self::warn;
				break;

			case 'N':
				$type = 'Notice';
				$col = self::yellow;
				$img = self::warn;
				break;

			case 'E':
				$type = 'Error';
				$col = self::red;
				$img = self::error;
				break;

			case 'D':
				$type = 'Stop';
				$col = self::red;
				$img = self::stop;
				break;

			case 'S':
				$type = 'Success';
				$col = self::green;
				$img = self::success;
				break;
		endswitch;

		$html = '<p style="padding: 1em; text-align:center; background-color: ' . $col . ';" onclick="style.display = \'none\';">';
		$html .= '<span style="float: left;">' . $img . '</span>' . $text . '<span style="float: right;">X</span></p>' . PHP_EOL;
		return $html;
	}

}
?>