<?php
/**
 * @todo add a searchable sqlite memory database to the registry
 * @todo add a persitable database to the regsitry posibly by extendig RTConfig
 * @todo add a frontend to add, update and delete database entries to the registry or to config
 */
class RTRegistry {

	/**
	 * Our array of options
	 * @access protected
	 */
	protected static $data = array();

	public function __construct() {
		// Subclasses should call this with parent::__construct();
	}

	public function __clone() {
		/**
		 * prevent cloning of the object: issues an E_USER_ERROR if this is attempted
		 */
		trigger_error('Cloning the registry is not permitted', E_USER_ERROR);
	}

	public function __destruct() {
		/**
		 foreach (self::$data as $key => $value) {
		 	if($key != 'default' && set persitant){
		 		// save to file??? or save to config
		 	}
		 }
		 */
	}

	public static function set($group, $key, $value = NULL) {
		if ($value === null && is_array($key)){
			self::setArray($group,$key);
		}else{
			self::$data[$group][$key] = $value;
		}
	}

	public static function setArray($group, array $array){
		foreach($array as $key => $value){
			self::set($group, $key, $value);
		}
	}

	public function get($group = null, $key = NULL) {
		if($group === NULL){
			return self::$data;
		}
		if($key === NULL && isset (self::$data[$group])){
			return self::$data[$group];
		}
		if (isset(self::$data[$group][$key])) {
			return self::$data[$group][$key];
		}
		return false;
	}

	public function has($group, $key = NULL) {
		if(isset(self::$data[$group])) {
			if($key === NULL){
				return TRUE;
			}
			if (isset(self::$data[$group][$key])) {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function count($group){
		if(isset(self::$data[$group])){
			return count(self::$data[$group]);
		}
		return 0;
	}

	public function del($group = null, $key = NULL) {
		if($group === null){
				self::$data = array();
				return TRUE;
		}
		if(isset(self::$data[$group])) {
			if($key === NULL){
				unset(self::$data[$group]);
				return TRUE;
			}
			if (isset(self::$data[$group][$key])) {
				unset(self::$data[$group][$key]);
				return TRUE;
			}
		}
		return FALSE;
	}

	public function __set($key, $value) {
		$this->set('default', $key, $value);
	}

	public function __get($key) {
		return $this->get('default', $key);
	}

	public function __isset($key) {
		return $this->has('default', $key);
	}

	public function __unset($key) {
		$this->del('default', $key);
	}

	public function getAll() {
		return self::$data;
	}
}
?>
