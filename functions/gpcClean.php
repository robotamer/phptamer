<?php

function gpcClean($clean){
    // clean superglobals
    if(!is_array($clean))
    {
        $clean = htmlentities($clean,ENT_QUOTES,'UTF-8');
    }else{
        foreach ($clean as $key => $value)
            $clean[$key] = gpcClean($value);
    }
    return $clean;
}
