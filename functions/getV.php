<?php

//echo getV('sLocale','US');
// 'config_array'

function getV($table, $id, $key = NULL)
{
	if( ! isset($_SESSION["vdata"])) $_SESSION["vdata"] = array('counter'=>1,'backend'=>'F3');
	$backend = $_SESSION["vdata"]['backend'];
	$_SESSION["vdata"]['counter']++;
	if(isset($vdata[$table][$id])){
		if($key === NULL) {
			return $vdata[$table][$id];
		}else{
			return $vdata[$table][$id][$key];
		}
	}

	if($backend == 'KvLite'){
		$data = KvLite::get($id, $key);
	}elseif($backend == 'F3'){
		$data = F3::get($table);
		if($key === null) $data = $data[$id];
		$data = $data[$id];
	}elseif(strstr($backend, '_') == '_array'){
		//include_once LIB.'RoboTamer/class/Load.php';
		$data = Load::getArray($id, $key, strstr($backend, '_',TRUE));
	}else{
		trigger_error("Backend not supported!");
		die;
	}
	return $data;
}

?>