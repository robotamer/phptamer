<?php
function autoload($className) {

    $className = ltrim($className, '\\');
    $fileName = '';
    $nameSpace = '';

    if (preg_match('/[^a-z0-9\\/\\\\_.:-]/i', $className)) {
        throw new Exception('Security check: Illegal character in filename');
    }

    if (class_exists($className, false) || interface_exists($className, false))
        return;

    if ($lastNsPos = strripos($className, '\\')) {
        $nameSpace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $nameSpace) . DIRECTORY_SEPARATOR;
    }

    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    include $fileName;

}

# Silently start the autoloader
spl_autoload_register('autoload');
?>
