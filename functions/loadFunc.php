<?php

/**
 *
 *
 * @category     RoboTamer
 * @author       Dennis T Kaplan
 * @copyright    Copyright (c) 2011, Dennis T Kaplan
 * @license      http://www.RoboTamer.com/license.php
 * @link         http://www.RoboTamer.com
 */
function loadFunc($str)
{
    $str = str_replace(',',' ',$str);
    $str = preg_replace('/\s\s+/', ' ', $str);

    $array = explode(' ',$str);
    foreach($array as $v){
        if ( !function_exists($v)) {
        	//echo __dir__.DIRECTORY_SEPARATOR.$v.'.php'.BR;
            include(dirname(__FILE__).DIRECTORY_SEPARATOR.$v.'.php');
        }
    }
}
?>
