<?php

/**
 *
 *
 * @category     RoboTamer
 * @author       Dennis T Kaplan
 * @copyright    Copyright (c) 2011, Dennis T Kaplan
 * @license      http://www.RoboTamer.com/license.php
 * @link         http://www.RoboTamer.com
 */
function loadF($functions)
{
    $functions = str_replace(',',' ',$functions);
    $functions = preg_replace('/\s\s+/', ' ', $functions);
    $functions = explode(' ',$functions); // Now Array

    foreach($functions as $functionName)
    {
        if( ! function_exists($functionName))
        {
            $file = PHPL.DS.'RoboTamer'.DS.'func'.DS.$functionName.'.php';
            if(extension_loaded('apc'))
            {
                $result = apc_fetch('F_'.$functionName);

                if($result == FALSE)
                {
                    $result = php_strip_whitespace($file);
                    apc_store('F_'.$functionName, $result, 3600);
                }
                eval( " ?> $result <?php ");
                unset($result);
            }
            else
            {
                include($file);
            }
        }
    }
}
?>
