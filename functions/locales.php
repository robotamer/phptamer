<?php
if(isset($_SERVER['SHELL'])){
	include './../bootroot.php';
	loadFunc('gpcClean');
	getCurrency();
}

trigger_error('Switch to class Localize()');

function languageLinks(){
	$locale = getLocale();
	$locales = KvLite::get('locales');
	$r='';
	foreach($locales as $k=>$v){
		$link = Uri::addSubDomain($k);
		$l='<img src="/asset/icon/flag-'.$k.'.jpeg" border="0">';
		$r.='<a href="'.$link.'" title="'.$k.'">'.$l.'</a>'.PHP_EOL.' | ';
	}
	$r=rtrim($r,"| ");
	return $r;
}

function getLocalePrice($price, $currency = NULL)
{
	$oldlocale = getLocale();
	if($currency === NULL)$currency = getCurrency();
	$locale = F3::get('localeByCurrency',$currency);
	setlocale(LC_ALL, $locale);
	$rate  = F3::get('xRate');//KvLite::get('xr',$currency);
	$price = $rate[$currency]*$price;//money_format('%n',$rate*$price);
	setlocale(LC_ALL, $oldlocale);
	return $price;
}

function getCurrency(){
	$currency = '';
	$return = 'USD';
	if(isset($_COOKIE['Currency']))
		$currency=gpcClean($_COOKIE['Currency']);
	$locales = F3::get('xLocale');
	foreach($locales as $v){
		$c=getCurrSym($v, 1);
		if($c == $currency)
			return $c;
	}
	return $return;
}

function currencyLinks(){
	$locale = getLocale();
	$locales = KvLite::get('localeLong');
	$r='';
	$self = $_SERVER['PHP_SELF'];
	foreach($locales as $k=>$v){
		$c=getCurrSym($v, 1);
		$s=($v == $locale)?'selected':'select';
		$js="setCookie('Currency','$c',365);";
		$r.='<a href="#" onclick="'.$js.'" title="'.$c.'">'.$c.'</a> | ';
	}
	$r=rtrim($r,' |');
	return $r;
}

function getCurrSym($locale, $int = 1){
	$orgLocal = getLocale();
	$locales = F3::get('xLocale');
	setlocale(LC_ALL, $locale);
	$localeconv = localeconv();
	setlocale(LC_ALL, $orgLocal);
	return $int == 1
		? trim($localeconv['int_curr_symbol'])
		: trim($localeconv['currency_symbol']);
}

function getLocale($category = LC_ALL){
	return setlocale($category, NULL);
}
/*
 * The locales() is set so the system doesn't try to reload these
 * functions with loadFunc()
 */
function locales(){}
/*
foreach($locales as $l){
	echo getCurrSym($l, 0).BR;
}
*/
//system('locale -a');

?>