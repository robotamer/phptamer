<?php

function jig_parse_csv($cvs_file, $jig_file, $delimiter = '|')
{
	F3::set('DB',new FileDB(DROOT,FileDB::FORMAT_Plain));
	$db = new Jig($jig_file);
	$row = 0;
	if (($handle = fopen($cvs_file, "r")) !== FALSE) {
		while (($line = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
			if($row == 0)
			{
				if(empty($line)) continue;
				$field_names = $line;
			}else{
				foreach($field_names as $key => $f)
				{
					$db->$f = $line[$key];
				}
				$db->save();
			}
			$row++;
		}
		fclose($handle);
		return $handle;
	}else{
		return $handle;
	}
}
?>