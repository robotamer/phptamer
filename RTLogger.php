<?php

/**
 * The RoboTamer Logger
 *
 *
 * The MIT License (MIT)
 *
 * Copyright © 2012 RoboTamer http://robotamer.github.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

if (PHP_SAPI == 'cli') {
	defined('DEVELOPER_EMAIL') || define('DEVELOPER_EMAIL', 'tamer@robotamer.com');
	#defined('DEVELOPER_EMAIL') || exit;
	Logger::mailit();
	exit;
}

/**
 * Logger
 *
 * Logger::set('testle');
 * echo Logger::gethtml('15');
 *
 * @category   Logger
 * @package    RoboTamer
 * @author     Dennis T Kaplan
 * @copyright  Copyright (c) 2008 - 2012, Dennis T Kaplan
 * @license    http://robotamer.github.com
 * @link       http://robotamer.github.com
 * @todo         integrate php error_log()
 * @todo         find a better solution for dev email, maybe ini file
 * @todo         create a RT folder before using /tmp
 */
class RTLogger
{

	private static $instance = NULL;
	protected $db = NULL;

	const developer_email = NULL;

	/**
	 * String to be used as subject line
	 * of email notification
	 *
	 * @var string
	 */
	const EMAIL_SUBJECT = 'Error on your website';

	public function __construct($msg = null, $level = 'Notice')
	{
		if($msg) self::set($msg, $level);
	}

	public function __destruct()
	{
		if (PHP_SAPI != 'cli') {
			exec('nohup php ' . __FILE__ . ' &');
		} else {
			$log = self::getInstance();
			if (isset($log->db)) {
				unset($log->db);
			}
		}
	}

	public function __clone()
	{
		trigger_error('Cloning is not allowed. ', E_USER_ERROR);
	}

	// Logging Functions
	public static function set($msg, $level = 'Notice')
	{
		if (is_array($msg) || is_object($msg)) {
			$msg = self::varDump($msg);
		}
		if (strtolower($level) != 'notice' && strtolower($level) != 'info' && DEBUG !== FALSE)
			self::emailError($msg);
		//loadF('randString');
		$db = self::db();
		$msg = $db->quote($msg);
		defined('LOGRAND') || define('LOGRAND', self::randString('4'));
		$sql = "INSERT INTO log (session, level, message) VALUES ('" . LOGRAND . "', '$level', $msg);";
		//echo $sql;die;
		$db->exec($sql);
	}

	public static function clear()
	{
		$db = self::db();
		$sql = "DELETE FROM log;";
		$db->exec($sql);
	}

	public static function gethtml($items = 20)
	{
		$db = self::db();
		$sql = "SELECT * FROM log ORDER BY added DESC LIMIT $items";
		$html = '<table border="1" cellpadding="5" cellspacing="0" class="center">';
		$html .= '<tr><th>Date Time</th><th>Session</th><th>Level</th><th>Message</th></tr>';

		$i = 0;
		foreach ($db->query($sql, PDO::FETCH_OBJ) as $row) {
			($i & 1) ? $color = '#ffffff' : $color = '#d5d5d5';
			$html .= '<tr style="background-color: ' . $color . '">' . PHP_EOL;
			$html .= "\t<td>$row->added</td>" . PHP_EOL;
			$html .= "\t<td>$row->session</td>" . PHP_EOL;
			$html .= "\t<td>$row->level</td>" . PHP_EOL;
			$html .= "\t<td>$row->message</td>" . PHP_EOL;
			$html .= "</tr>" . PHP_EOL;
			$i++;
		}

		$html .= '</table>';
		return $html;
	}

	protected static function getInstance()
	{
		if (self::$instance === NULL) {
			$class = __CLASS__;
			self::$instance = new $class;
		}
		return self::$instance;
	}

	protected static function db()
	{

		$log = self::getInstance();

		if (isset($log->db) && $log->db !== NULL)
			return $log->db;

		$dir = sys_get_temp_dir();
		$dbname = $dir . DIRECTORY_SEPARATOR . 'phplogs.sqlite';
		try {
			$log->db = new PDO('sqlite:' . $dbname);
			if (defined('DEBUG') && DEBUG !== FALSE && PHP_SAPI != 'cli') {
				//chmod($dbname, 0666);
				//$log->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		} catch (PDOException $e) {
			trigger_error("PDO Sqlite3 Connection with config failed:<br />$dbname<br />" . $e->getMessage());
			exit;
		}
		if (defined('DEBUG') && DEBUG !== FALSE) {
			$sql = "SELECT name FROM sqlite_master WHERE type='table' UNION ALL SELECT name FROM sqlite_temp_master WHERE type='table' ORDER BY name";
			$result = $log->db->query($sql);
			$tables = array ();
			foreach ($result as $row)
				$tables[] = current($row);
			if (!in_array('log', $tables)) {
				$log->db->exec("CREATE TABLE log (added DATETIME, session TEXT, level TEXT, message TEXT)");
				$log->db->exec("CREATE TRIGGER log_insert_time AFTER INSERT ON log BEGIN UPDATE log SET added = datetime('NOW','UTC') WHERE rowid = last_insert_rowid(); END;");
			}
			if (!in_array('mail', $tables)) {
				$log->db->exec("CREATE TABLE mail (added DATETIME, email TEXT, headers TEXT, subject TEXT, message TEXT)");
				$log->db->exec("CREATE TRIGGER mail_insert_time AFTER INSERT ON mail BEGIN UPDATE mail SET added = datetime('NOW','UTC') WHERE rowid = last_insert_rowid(); END;");
			}
			$error = $log->db->errorInfo();
			if(!empty($error[1])){
				var_dump($error);
			}
		}
		return $log->db;
	}

	/**
	 * Sends email message to developer
	 * if message contains error pattern
	 */
	protected static function emailError($msg)
	{
		$devEmail = trim(self::getDevEmail());
		$devEmail = trim($devEmail, "'");

		if (empty($devEmail)) {
			return;
		}

		if (isset($_SERVER) && is_array($_SERVER)) {
			$msg .= PHP_EOL . '-----------------------------------------------------';
			$msg .= PHP_EOL . 'HTTP_HOST:       ' . self::getServerVar('HTTP_HOST');
			$msg .= PHP_EOL . 'SERVER_NAME:     ' . self::getServerVar('SERVER_NAME');
			$msg .= PHP_EOL . '-----------------------------------------------------';
			$msg .= PHP_EOL . 'SCRIPT_NAME:     ' . self::getServerVar('SCRIPT_NAME');
			$msg .= PHP_EOL . 'SCRIPT_FILENAME: ' . self::getServerVar('SCRIPT_FILENAME');
			$msg .= PHP_EOL . '-----------------------------------------------------';
			$msg .= PHP_EOL . 'HTTP_USER_AGENT: ' . self::getServerVar('HTTP_USER_AGENT');
			$msg .= PHP_EOL . 'HTTP_REFERER:    ' . self::getServerVar('HTTP_REFERER');
			$msg .= PHP_EOL . '-----------------------------------------------------';
			$msg .= PHP_EOL . 'REQUEST_METHOD:  ' . self::getServerVar('REQUEST_METHOD');
			$msg .= PHP_EOL . 'REQUEST_URI:     ' . self::getServerVar('REQUEST_URI');
			$msg .= PHP_EOL . 'REMOTE_ADDR/IP:  ' . self::getServerVar('REMOTE_ADDR');
			$msg .= PHP_EOL . '-----------------------------------------------------';
		}

		/**
		 * Add hight priority to email headers
		 * for error messages of certain types (real errors, no notices)
		 */
		$headers = '';
		$headers .= 'X-Mailer: RoboTamer' . PHP_EOL;
		$headers .= 'X-Priority: 1' . PHP_EOL;
		$headers .= 'Importance: High' . PHP_EOL;
		$headers .= 'X-MSMail-Priority: High';


		$db = self::db();
		$sql = sprintf('INSERT INTO mail (email, headers, subject, message) VALUES ("%s", "%s", "%s", "%s");', $devEmail, $headers, self::EMAIL_SUBJECT, $db->quote($msg));
		$db->exec($sql);
	}

	public static function mailit()
	{

		$db = self::db();
		$sql = "SELECT * FROM mail ORDER BY added ";

		foreach ($db->query($sql, PDO::FETCH_OBJ) as $row) {
			foreach ($row as $k => $v) {
				$row->$k = trim($v, "'");
				$row->$k = trim($v);
			}
			mail($row->email, $row->subject, $row->message, $row->headers);
		}
		$db->exec('DELETE FROM mail');
	}

	/**
	 * Get value from global $_SERVER array
	 * if it exists, otherwise return 'Not Available'
	 *
	 * @param string $var
	 * @return string value of $var or empty string
	 */
	protected static function getServerVar($var)
	{
		return (array_key_exists($var, $_SERVER)) ? $_SERVER[$var] : 'N/A';
	}

	public static function setDevEmail($email)
	{
		defined('DEVELOPER_EMAIL') || define('DEVELOPER_EMAIL', $email);
	}

	/**
	 * Get email address of developer
	 *
	 * @return mixed email address of developer or false
	 */
	public static function getDevEmail()
	{
		return defined('DEVELOPER_EMAIL') ? DEVELOPER_EMAIL : FALSE;
	}

	public static function varDump($param)
	{
		ob_start();
		var_dump($param);
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	/**
	 * Readable param is good for captcha for example
	 *
	 * randString(rand(10,15));
	 *
	 * @category     RoboTamer
	 * @author       Dennis T Kaplan
	 * @copyright    Copyright (c) 2011, Dennis T Kaplan
	 * @license      http://www.RoboTamer.com/license.php
	 * @link         http://www.RoboTamer.com
	 *
	 * @version      1.3
	 * @param        string $length
	 * @param        bool   $readable
	 * @return       string
	 */
	public static function randString($length=6, $readable=FALSE)
	{
		if ($readable == FALSE) {
			$char = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		} else {
			$char = '23479ABCDEFHJLMNQRTUVWXYZabcefghijkmnopqrtuvwxyz';
		}
		$c = '';
		for ($i = 1; $i <= $length; $i++) {
			$c .= $char;
		}
		return substr(str_shuffle($c), 0, $length);
	}

}
?>
