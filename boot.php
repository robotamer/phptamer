<?php

date_default_timezone_set('UTC');
ini_set('short_open_tag', 'Off');
ini_set('asp_tags', 'On');
ini_set('filter.default', 'full_special_chars');
ini_set('intl.default_locale', 'en-US');
ini_set('iconv.input_encoding', 'UTF-8');
ini_set('iconv.internal_encoding', 'UTF-8');
ini_set('iconv.output_encoding', 'UTF-8');

define('PHPL', realpath(dirname(__FILE__).'/../'));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('HOSTNAME') || define('HOSTNAME', trim(shell_exec('hostname -f')));

if (PHP_SAPI == 'cli') {
	$_SERVER["SERVER_NAME"] = HOSTNAME;
	$_SERVER['HTTP_HOST']   = HOSTNAME;
	defined('BR') || define('BR', PHP_EOL);
    defined('HR') || define('HR', BR . "_______________________" . BR);
} else {
    defined('BR') || define('BR', "<br />" . PHP_EOL);
    defined('HR') || define('HR', "<hr />" . PHP_EOL);
}

include dirname(__FILE__).'/functions/loadFunc.php';
loadFunc('autoload addInclude');
addInclude(realpath(dirname(__FILE__)), FALSE);
include dirname(__FILE__).'/RTSingleton.php';
?>