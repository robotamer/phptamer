<?php
/**
 * The RoboTamer Logger
 * 
 * 
 * The MIT License (MIT)
 * 
 * Copyright © 2012 RoboTamer http://robotamer.github.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. 
 */

/**
 * Messanger
 * 
 * @category   Messanger
 * @package    RoboTamer
 * @author     Dennis T Kaplan
 * @copyright  Copyright (c) 2008 - 2012, Dennis T Kaplan
 * @license    http://robotamer.github.com
 * @link       http://robotamer.github.com
 */
class RTMessanger {

    public static $msgss = array();

    /**
     * Add a message to the message array (adds to the user's session)
     * @param string  $type    These are class names for Bootstrap's messaging classes: info, error, success, warning
     * @param string $message  The message you want to add to the list
     */
    public static function add($message, $type = 'info') {
        $_SESSION['messages'][$type][] = $message;
    }

    /**
     * Pull back those messages from the session
     * @return array
     */
    public static function get() {
        return isset($_SESSION['messages']) ? $_SESSION['messages'] : FALSE;
    }

    /**
     * Gets all the messages from the session and formats them accordingly for Twitter bootstrap.
     * @return string
     */
    public static function getHtml() {
        $output = FALSE;
        if (isset($_SESSION['messages'])) {
            $output = '<div class="ui-widget" id="dialog">';
            foreach ($_SESSION['messages'] as $type => $msgs) {
                if (is_integer($type))
                    $type = 'error';
                if (is_array($msgs)) {
                    foreach ($msgs as $msg) {
                        if ($type == 'error') {
                            $output .= '<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">';
                            $output .= '<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>';
                        } else {
                            $output .= '<div class="ui-state-highlight ui-corner-all">';
                            $output .= '<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
                        }
                        $output .= $msg . "</p></div>\n";
                    }
                } else {
                    $output .= '<div class="ui-state-highlight ui-corner-all">';
                    $output .= '<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
                    $output .= $msg . "</p></div>\n";
                }
            }
            $output .= '</div>';
            unset($_SESSION['messages']);
        }
        return $output;
    }
}
?>
