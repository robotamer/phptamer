<?php

class RTUser
{

	private static $db;

	public static function check($groups)
	{
		if (empty($_SESSION['auth']))
			self::login();
		if (array_key_exists(999, $_SESSION['auth']['groups']) || in_array('Admin', $_SESSION['auth']['groups']))
			return true;
		$groups = explode(' ', trim($groups));
		foreach ($groups as $v) {
			if (in_array($v, $_SESSION['auth']['groups']) || array_key_exists($v, $_SESSION['auth']['groups'])) {
				return TRUE;
			} else {
				$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
				header("$protocol 401 Unauthorized");
				header('Location: /401.php');
				exit;
			}
		}
	}

	public function db()
	{
		try {
			if (empty(self::$db)) {
				$dbname = ROOT . '/var/db/users.db3';
				self::$db = new PDO('sqlite:' . $dbname);
			}
		} catch (PDOException $e) {
			self::$db = NULL;
			trigger_error("PDO Sqlite3 Connection failed:<br />$dbname<br />" . $e->getMessage());
		}
		return self::$db;
	}

	/**
	 * End the database connection before the object is destroyed
	 */
	public function __destruct()
	{
		if (is_object(self::$db))
			self::$db = NULL;
	}

	public static function getError()
	{
		return is_object(self::$db) ? self::$db->errorInfo() : FALSE;
	}

	private static function logError()
	{
		$error = self::getError();
		if (!empty($error[1])) {
			S::RTLogger()->set($error[2], 'Error');
		}
	}

	#get the number of rows in a result
	public static function num_rows($query)
	{
		self::db();

		# create a prepared statement
		$stmt = self::$db->prepare($query);

		if ($stmt) {
			# execute query
			$stmt->execute();

			return $stmt->rowCount();
		}
	}

	public static function hasUser($uid)
	{
		self::db();
		$sql = 'SELECT username FROM users WHERE username = :username LIMIT 1';
		$sth = self::$db->prepare($sql);
		$sth->bindValue(':username', $uid, PDO::PARAM_STR);
		$sth->execute();
		$return = $sth->fetchColumn();
		self::logError();
		return $return;
	}

	public static function addUser($username, $email = NULL)
	{
		self::db();
		if ($email === NULL) {
			$sql = 'INSERT INTO users (username) VALUES(:username)';
		} else {
			$sql = 'INSERT INTO users (username,email) VALUES(:username,:email)';
		}
		$sth = self::$db->prepare($sql);
		$sth->bindValue(':username', $username, PDO::PARAM_STR);
		if ($email !== NULL) {
			$sth->bindValue(':email', $email, PDO::PARAM_STR);
		}
		$return = $sth->execute();
		self::logError();
		return $return;
	}

	public static function updateUser($usr)
	{
		$groups = array ();
		extract($usr, EXTR_OVERWRITE);
		$g = ' ';
		foreach ($groups as $v) {
			$g .= $v . ' ';
		}
		$groups = $g;
		unset($g, $v, $k, $usr);
		$status = empty($status) ? NULL : 1;
		self::db();
		$sql = 'UPDATE users SET password=:password,name=:name,email=:email,groups=:groups,status=:status WHERE username=:username';
		$sth = self::$db->prepare($sql);
		$sth->bindValue(':username', $username, PDO::PARAM_STR);
		$sth->bindValue(':password', $password, PDO::PARAM_STR);
		$sth->bindValue(':name', $name, PDO::PARAM_STR);
		$sth->bindValue(':email', $email, PDO::PARAM_STR);
		$sth->bindValue(':groups', $groups, PDO::PARAM_STR);
		$sth->bindValue(':status', $status, PDO::PARAM_INT);
		$return = $sth->execute();
		self::logError();
		return $return;
	}

	public static function getUser($username)
	{
		self::db();
		$sql = 'SELECT * FROM users WHERE username = :user LIMIT 1';
		$sth = self::$db->prepare($sql);
		$sth->bindValue(':user', $username, PDO::PARAM_STR);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_OBJ);
		if ($result !== false) {
			$result->groups = trim($result->groups);
			if (!empty($result->groups)) {
				$result->groups = explode(' ', $result->groups);
				$usergroups = self::getGroupNames();
				foreach ($result->groups as $v)
					$g[(INT) $v] = $usergroups[(INT) $v];
				$result->groups = $g;
			} else {
				$result->groups = array ();
			}
		}
		return $result;
	}

	public static function getGroupUsers($group = 1)
	{
		self::db();

		# int strpos ( string $haystack , mixed $needle [, int $offset = 0 ] )
		self::$db->sqliteCreateFunction('strpos', 'strpos', 2);
		$sql = sprintf('SELECT username FROM users WHERE strpos(groups," %d ")', $group);
		$sth = self::$db->query($sql);
		while ($row = $sth->fetchColumn()) {
			$result[] = $row;
		}
		return $result;
	}

	public static function getGroupNames()
	{
		return S::R()->get('auth', 'groups');
	}

	/**
	 * Get the groups a user bilongs too
	 */
	public static function getGroups($username)
	{
		self::db();
		$sql = 'SELECT groups FROM users WHERE username = :user LIMIT 1';
		$sth = self::$db->prepare($sql);
		$sth->bindValue(':user', $username, PDO::PARAM_STR);
		$sth->execute();
		$groups = $sth->fetchColumn();
		return self::assign_name_2_groups($groups);
	}

	protected static function assign_name_2_groups($dbgroups)
	{
		$usergroups = self::getGroupNames();
		$groupsarray = explode(' ', trim($dbgroups));
		foreach ($groupsarray as $v) {
			$groups[$v] = $usergroups[$v];
		}
		return $groups;
	}

	public static function authenticate($username, $password)
	{
		$sql = 'SELECT count(*) FROM users WHERE username = :user AND password = :pass LIMIT 1';
		self::db();
		$sth = self::$db->prepare($sql);
		$sth->bindValue(':user', $username, PDO::PARAM_STR);
		$sth->bindValue(':pass', $password, PDO::PARAM_STR);
		$sth->execute();
		$result = $sth->fetchColumn();
		return $result > 0 ? true : false;
	}

	public static function doLogin($uid)
	{
		$_SESSION['auth']['username'] = $uid;
		$_SESSION['auth']['groups'] = self::getGroups($uid);
		RTUri::setWorkSegments();
		header('Location: ' . RTUri::url('settings','lobby'));
	}

	public static function login()
	{
		if (!isset($_SESSION['auth']))
			$_SESSION['auth'] = array ();
		if (empty($_SESSION['auth']['username'])) {
			if (isset($_SESSION['auth']['logincount']) && $_SESSION['auth']['logincount'] > 3 && empty($_SESSION['auth']['captcha'])) {
				header('Location: /captcha.php');
				exit;
			}
			if (!empty($_REQUEST['username']) && !empty($_REQUEST['password'])) {
				if (self::authenticate($_REQUEST['username'], $_REQUEST['password'])) {
					self::doLogin($_REQUEST['username']);
				} else {
					empty($_SESSION['auth']['logincount']) ? $_SESSION['auth']['logincount'] = 1 : $_SESSION['auth']['logincount']++;
					$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
					header("$protocol 401 Unauthorized");
					header('Location: /401.php');
					exit;
				}
			} else {
				$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
				header("$protocol 401 Unauthorized");
				header('Location: /401.php');
				exit;
			}
		} else {
			return true;
		}
	}

	public static function logout()
	{
		unset($_SESSION['auth']['username']);
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"]);
		}
		session_destroy();
		session_write_close();
		header('Location: /');
	}

}
/**
  DROP TABLE IF EXISTS users;
  BEGIN TRANSACTION;
  CREATE TABLE users (username TEXT UNIQUE, password TEXT, name TEXT, email TEXT, groups TEXT, status INTEGER, created, updated, data);
  CREATE TRIGGER users_insert_time AFTER INSERT ON users BEGIN UPDATE users SET created = datetime('NOW','UTC'), password = hex(randomblob(3)), status = 1 WHERE rowid = last_insert_rowid(); END;
  CREATE TRIGGER users_update_time AFTER UPDATE ON users BEGIN UPDATE users SET updated = datetime('NOW','UTC') WHERE OLD.oid = oid; END;
  INSERT INTO users VALUES('tamer','12345','Admin','admin@example.com',' 1 2 3 999 ',NULL,NULL,NULL,NULL);
  COMMIT;
 */
?>