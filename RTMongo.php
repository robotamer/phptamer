<?php
class RTMongo {
	//	use RTMagic;

	/**
	 * Mongo connection resource
	 * @object of type Mongo
	 */
	protected $mongo;

	/**
	 * Object MongoDB
	 * @object of type MongoDB
	 */
	protected $db;

	public function __construct() {

		if (!extension_loaded('mongo'))
			throw new Exception("Unable to load Mongo DB, extention not loaded.");
	}

	public function db($name = 'default') {

		if (!is_object($this->mongo))
			$this->connect();
		$options = $this->options();

		if ($name == 'default')
			$name = $options['name'];

		return $this->mongo->$name;
	}

	private function connect($db = 'default') {

		$options = $this->options();
		$set = NULL;
		if ($options['host'] !== NULL) {
			if ($options['port'] !== NULL)
				$port = ":{$options['port']}";
			$set = "mongodb://{$options['user']}:{$options['pass']}@{$options['host']}$port}";
			if ($options['reps'] !== NULL)
				$set .= ", array('replicaSet' => true)";
		}

		try {
			//$this->mongo = new Mongo("localhost:27017", array("persist" => "x"));
			if ($set !== NULL) {
				$this->mongo = new Mongo($set);
			} else {
				$this->mongo = new Mongo();
			}

		} catch(MongoConnectionException $e) {
			RTLogger::set('RTMongo: ' . $e->getMessage(),'error');
			RTMessanger::add('error','Could not connect to the database'); 
		} catch(MongoException $e) {
			RTLogger::set('RTMongo: ' . $e->getMessage(),'error');
			RTMessanger::add('error','Could not connect to the database'); 
		}
	}

	public function options() {

		if (isset($this->options) && !empty($this->options))
			return $this->options;

		$this->set('name', defined('MONGODB_NAME') ? MONGODB_NAME : 'robotamer');
		$this->set('host', defined('MONGODB_HOST') ? MONGODB_HOST : NULL);
		//'localhost');
		$this->set('user', defined('MONGODB_USER') ? MONGODB_USER : NULL);
		$this->set('pass', defined('MONGODB_PASS') ? MONGODB_PASS : NULL);
		$this->set('port', defined('MONGODB_PORT') ? MONGODB_PORT : NULL);
		//'27017');
		$this->set('reps', defined('MONGODB_REPS') ? MONGODB_REPS : NULL);
		return $this->options;
	}

	public function getid($obj) {
		/**
		 * Not tested!!!
		 */
		if (is_object($obj)) {
			foreach ($obj as $key => $value) {
				if ($key == '$id') {
					return (string)$value;
				}
			}

		}

		if (is_array($obj)) {
			foreach ($obj as $key => $value) {
				if ($key == '_id') {
					$value = (array) $value;
					return $value['$id'];
				}
			}
		}
		$e = __('Error: MongoDB getid arg not an object or array').BR;
		trigger_error($e, E_USER_ERROR);
	}

	public function mkid($length = 12) {
		$char = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$c = '';
		for ($i = 1; $i <= $length; $i++) {
			$c .= $char;
		}
		return substr(str_shuffle($c), 0, $length);
	}

}
?>
