<?php // Uri

/**
 * An open source application development framework for PHP 5.2 or newer
 *
 * @package      TaMeR
 * @author       Dennis T Kaplan
 * @copyright    Copyright (c) 2008 - 2010, Dennis T Kaplan
 * @license      http://tamer.pzzazz.net/license.html
 * @link         http://tamer.pzzazz.net
 */

/**
 * Name: RTUri
 * Description
 *
 * @author     Dennis T Kaplan
 */

/****************************
echo '<pre>';
echo '<br />appPath: ';
echo RTUri::appPath();
echo BR . 'appFile: ' . RTUri::appFile();
echo '<br />appFolder: ';
echo RTUri::appFolder();
echo '<br />appName: ';
echo RTUri::appName();
echo '<br />className: ';
echo RTUri::className();
echo '<br />actionName: ';
echo RTUri::actionName();
echo '<br />segments: ';
print_r(RTUri::get());
echo '</pre>';
******************************/

class RTUri
{
	protected static $domain;
	protected static $vars;
	protected static $envsegs;
	protected static $worksegs;

	final private function __construct() {}

	public static function init(){
		self::setEnvSegments();
		self::setWorkSegments();
	}


	public static function protocol() {
		return (getenv('HTTPS') == 'on') ? 'https' : 'http';
	}

	public static function domain() {
		if(empty(self::$domain)){
			$host = array_reverse(explode('.', $_SERVER['HTTP_HOST']));
			self::$domain = $host[1] . '.' . $host[0];
		}
		return self::$domain;
	}

	public static function subdomain() {
		$host = array_reverse(explode('.', $_SERVER['HTTP_HOST']));
		unset($host[0],$host[1]);
		$host[2] = isset($host[2]) ? $host[2] : 'www';
		$host = array_reverse($host);
		$sub = '';
		foreach($host as $v){
			$sub .= $v . ' ';
		}
		return str_replace(' ', '.', trim($sub));
	}

	/* Adds a subdomain to the domain returning
	 * http://subdomain.exsample.com/index.php
	 */
	public static function addSubdomain($sub) {
		return self::protocol() . '://' . $sub . '.' . self::domain() . self::selfUrl();
	}

	public static function baseUrl() {
		return self::protocol() . '://' . self::subdomain() . '.' . self::domain();
	}

	public static function selfUrl() {
		// /admin/debug.php and nothing after just the file
		$selfurl = getenv('SCRIPT_NAME');
		return $selfurl;
		//strstr($selfurl, '.php', TRUE);
	}

	public static function fullUrl() {
		$s = '/';
		return self::baseUrl() . $s . trim(self::selfUrl(), '/');
	}

	public static function setUrlSegments($segments = NULL) {
		$options = '';
		$s = '/';
		$segments = ($segments === NULL) ? self::get() : $segments;
		if (!empty($segments)){
			foreach ($segments as $k => $v) {
				if (!is_int($k)) {
					$options .= $s . $k . '=' . $v;
				}
			}
		}
		return $options;
	}

	public static function url($file = NULL, $action = NULL, $segments = NULL) {
		$s = '/';

		if($file === NULL)   $file = strtolower(self::className());
		if($action === NULL && $file == strtolower(self::className())) $action = self::actionName();

		if ( !empty($file))   $file =   $s . $file;
		if ( !empty($action)) $action = $s . $action;

		$options = self::setUrlSegments($segments);

		return self::baseUrl() . $file . $action . $options;
	}

	public static function link($name, $file = NULL, $action = NULL,$title = NULL) {
		$file = ($file === NULL) ? strtolower(preg_replace('/ss+/i', '_', $name)) : $file ;
		$url = self::url($file, $action, self::get());
		self::del();

		$title = strpos($name, '>') ? (empty($title) ? '' : $title) : (empty($title) ? $name : $title);
		$name = ucfirst($name);
		return '<a href="' . $url . '" title="' . $title . '">' . $name . '</a>';
	}

	public static function appFolder() {
		/*
		 * Application Folder
		 * Removes file name and returns just the URL folder
		 * http://www.example.com/admin/test.php
		 * @return string /admin
		 */
		$haystack = getenv('SCRIPT_NAME');
		return substr($haystack, 0, strrpos($haystack, '/'));
	}

	public static function mvcPath() {
		return ROOT;
	}

	public static function appPath() {
		/*
		 * Full Application Path from server root
		 */
		return getenv('SCRIPT_NAME');
	}

	public static function appName() {
		$haystack = self::appFile();
		return substr($haystack, 0, strpos($haystack, '.'));
	}

	public static function appFile() {
		if(isset ($_SERVER['DOCUMENT_URI'])){

		}
		$haystack = getenv('SCRIPT_NAME');
		$array = array_reverse(explode('/', $haystack));
		return $array[0];
	}

	public static function className() {
		if(isset(self::$envsegs[0]) && strpos(self::$envsegs[0],'.') === FALSE){
			return ucwords(self::$envsegs[0]);
		}
		return NULL;
	}
	public static function actionName() {
		if(isset(self::$envsegs[1]) && strpos(self::$envsegs[1],'.') === FALSE ){
			return strtolower(self::$envsegs[1]);
		}
		return NULL;
	}

	public static function language() {
		$language = self::get('language');
		if ( !empty($language) && strlen($language) == 2) {
			include_once 'Zend/Locale.php';
			$list = Zend_Locale::getTranslationList('language', 'en');
			$language = array_key_exists($language, $list) ? $language : 'en';
		}
		return $language;
	}

	public static function pathInfo(){
		if (isset($_SERVER['PATH_INFO'])) {
			$path = $_SERVER['PATH_INFO'];
		} elseif (isset($_SERVER['REQUEST_URI'])) {
			$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH );
		}
		return isset($path) ? htmlentities(strip_tags($path,''), ENT_QUOTES, 'UTF-8') : '';
	}


	protected static function setEnvSegments() {
		$requestPathArray = array();
		$path = self::pathInfo();

		$requestPathArray = explode('/', trim(trim(urldecode($path)), '/'));
		foreach ($requestPathArray as $value) {
			if ( ! empty($value)) {
				if (strpos($value, '=') > 0) {
					list($k, $v) = explode('=', $value);
					$k = self::validateSegName(trim($k));
					$v = self::validateSegValue(preg_replace('# {1,}#', '_', trim($v)));
					self::$envsegs[$k] = $v;
					unset($k, $v);
				} else {
					self::$envsegs[] = self::validateSegName(trim($value));
				}
			}
		}
		return self::$envsegs;
	}

	public static function getEnvSegments(){
		return empty(self::$envsegs) ? self::setEnvSegments() : self::$envsegs;
	}

	public static function getEnvSegment($key) {
		if(isset(self::$envsegs[$key])) return self::$envsegs[$key];
	}

	public static function setWorkSegments() {
		$s = (array) self::getEnvSegments();
		foreach($s as $k=>$v){
			if( ! is_int($k)) self::$worksegs[$k] = $v;
		}
		if(empty(self::$worksegs['country']))  self::$worksegs['country']  = defined('COUNTRY')  ? COUNTRY  : 'US';
		if(empty(self::$worksegs['language'])) self::$worksegs['language'] = defined('LANGUAGE') ? LANGUAGE : 'en';
		return self::$vars = self::$worksegs;
	}

	/**
	 * User settings
	 *
	 * @param type mixed $key
	 * @param type mixed $value
	 * @return
	 */
	public static function setWorkSegment($key, $value = NULL) {
		if(is_array($key)){
			self::$worksegs = array_merge(self::$worksegs, $key);
		}else{
			self::$worksegs[$key] = $value;
		}
	}

	public static function getWorkSegments() {
		return empty(self::$worksegs) ? self::setWorkSegments() : self::$worksegs;
	}

	public static function set($key, $value) {
		self::$vars[$key] = $value;
	}

	public static function add($key, $value) {
		self::$vars[$key] = $value;
	}

	public static function has($key) {
		return array_key_exists($key, self::$vars);
	}

	public static function del($key = NULL) {
		if($key === NULL){
			self::$vars = self::getWorkSegments();
		}else{
			if(array_key_exists($key, self::$vars)) unset(self::$vars[$key]);
		}
	}

	public static function count() {
		return count(self::$vars);
	}

	public static function get($key = NULL) {
		if($key === NULL){
			return self::$vars;
		}else{
			if(array_key_exists($key, self::$vars)) return self::$vars[$key];
		}
	}

	/**
	 *
	 * @param string $string
	 * @return string error
	 */
	public static function validateSegName($string) {
		//if (ctype_alnum($string)) {
		if(preg_match("/^[A-Za-z0-9_.]*$/", $string)) {
			return $string;
		} else {
			trigger_error('Allowed are Alpha, Numeric and these _. ' . $string, E_USER_ERROR);
		}
	}

	public static function validateSegValue($string) {
		if(preg_match("/^[A-Za-z0-9(_.@#|)]*$/", $string)|| $string == '') {
			return $string;
		} else {
			trigger_error('Allowed are Alpha, Numeric and these _.@#*$() ' . $string, E_USER_ERROR);
		}
	}
}
?>